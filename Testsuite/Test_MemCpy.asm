    // Test Routine for the MemCpy Subroutine

    #define Include_MemCpy

    :BasicUpstart2(Main)

Main:

    sei

    :MemCpy($0400+40,$0400+[40*0]-1,40)
    :MemCpy($0400+40,$0400+[40*2]+1,40)
    :MemCpy($0400+40,$0400+[40*3]+2,40)
    :MemCpy($0400+40,$0400+[40*4]+3,40)
    :MemCpy($0400+40,$0400+[40*5]+4,40)
    :MemCpy($0400+40,$0400+[40*6]+5,40)
    :MemCpy($0400+40,$0400+[40*7]+6,40)
    :MemCpy($0400+40,$0400+[40*8]+7,40)
    :MemCpy($0400+40,$0400+[40*9]+8,40)
    :MemCpy($0400+40,$0400+[40*10]+9,40)
    :MemCpy($0400+40,$0400+[40*11]+10,40)

    cli
    rts

    .import source "../Library/Library.asm"



