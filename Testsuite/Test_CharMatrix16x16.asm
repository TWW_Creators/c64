    // Test Routine for 16x 16 Char Matrix Routine:
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    #define Include_CharMatrix16x16

    // Centre it
    .const CentreX = [40 - 16] / 2
	.const CentreY = round([25 - 16] / 2)
    .const Offset = [40 * CentreY] + CentreX

    :BasicUpstart2(Main)

Main:
    sei
	lda #BLACK
	sta $d020
	sta $d021                  // Screen and border = Black
	lda #WHITE                 // Text Color = White
	jsr $e536                  // ROM: Clear Screen & Set text color

    // Done manually via subroutine:
//    lda #$04 + >Offset
//    ldx #$00 + <Offset
//    ldy #BLUE
//    jsr CharMatrix16x16


    // Done with Macro:
	:CharMatrix16x16($0400 + Offset, BLUE)

    cli
    rts


    .import source "../Library/Library.asm"



