    // Test Routine for StackCall Routines:
    //
    //   - StackEntry
    //   - StackFix
    //   - StackExit
    //
    // By using the MemSet Subroutine
    // The MemSet Subroutine import and use the StackCall Routines

    #define Include_MemSet

    :BasicUpstart2(Main)

Main:
    sei
    :MemSet($0400,1000,$01)
    :MemSet($d800,500,$0b)
    :MemSet($d800+500,500,$07)
    cli
    rts                      // 24 bytes

/*  Custom routine for comparison:
    ldx #251
!:  lda #$01
    sta $0400-1,x
    sta $0400+250-1,x
    sta $0400+500-1,x
    sta $0400+750-1,x
    lda #$0b
    sta $d800-1,x
    sta $d800+250-1,x
    lda #$07
    sta $d800+500-1,x
    sta $d800+750-1,x
    dex
    bne !-
    rts                      // 35 bytes
*/
    .import source "../Library/Library.asm"



