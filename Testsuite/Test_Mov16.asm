    // Test Routine for Mov16 pseudocommand
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	// Check memory @ $1000 to verify results

    .import source "../Library/Library.asm"



                // Source: Immediate                   - Destination: Absolute
	:Mov16 #$1234 : $fffe


    // This will not work due to the stupid(!) label "ahead" issue where an .if statement
	// can't figgure out whathafuck unless a label is already resolved to a physical
	// memory address.

	// If we somehow could detect if the passed value is a label, perhaps we could
	// avoid the .if in the immediate section and use the regular version instead?
	.align $10  // Source: Immediate (Label Vector)    - Destination: Absolute
	:Mov16 #IRQ : $fffe

    .align $10  // Source: Zero Page (ABS)             - Destination: Zero Page (ABS)
	:Mov16 ZP_Reg1 : ZP_Reg2

    .align $10  // Source: Zero Page (ABS,X)           - Destination: Zero Page (ABS,Y (Illegal))
	:Mov16 ZP_Reg1,x : ZP_Reg2,y

    .align $10  // Source: Zero Page (ABS,Y (Illegal)) - Destination: Zero Page (ABS,X)
	:Mov16 ZP_Reg1,y : ZP_Reg2,x

    .align $10  // Source: Absolute                    - Destination: Absolute
	:Mov16 $1000 : $2000

    .align $10  // Source: Absolute,x                  - Destination: Absolute,y
	:Mov16 $1000,x : $2000,y

    .align $10  // Source: (Indirect ZP,x)             - Destination: (Indirect ZP),y
    :Mov16 (ZP_Reg1,x) : (ZP_Reg2),y

    .align $10  // Source: (Indirect ZP),y             - Destination: (Indirect ZP,x)
    :Mov16 (ZP_Reg1),y : (ZP_Reg2,x)

    .align $10  // Source: (Indirect ZP,x)             - Destination: (Indirect ZP,x)
    :Mov16 (ZP_Reg1,x) : (ZP_Reg2,x)

    .align $10  // Source: (Indirect ZP),y             - Destination: (Indirect ZP),y
    :Mov16 (ZP_Reg1),y : (ZP_Reg2),y

    // This one will produce only one LDA as the hi/lo byte of the immediate value is
	// the same.
    .align $10  // Source: #$1010 (IMM)                - Destination: Absolute
    :Mov16 #$1010 : $1000


    .pc = $abcd
.label IRQ = *