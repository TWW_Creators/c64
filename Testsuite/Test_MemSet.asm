    // Test Routine for the MemSet Subroutine

    #define Include_MemSet

    :BasicUpstart2(Main)

Main:
    sei
    :MemSet($0400,40,$00)
    :MemSet($0428,40,$40)
    :MemSet($d800,40,$01)
    :MemSet($d828,40,$05)
    cli
    rts

    .import source "../Library/Library.asm"



