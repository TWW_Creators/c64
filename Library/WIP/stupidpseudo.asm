
    .import source "../Library.asm"

    // Test Immediate
    :Mov16 #$1234 : $2000     ; nop ; nop ; nop  // OK!
    :Mov16 #$0101 : $d020     ; nop ; nop ; nop  // OK!

    // Test (ZP,x)
	:Mov16 ($02,x) : $1000    ; nop ; nop ; nop  // OK!
	:Mov16 $1000 : ($02,x)    ; nop ; nop ; nop  // OK!
	:Mov16 ($fb,x) : ($02,x)  ; nop ; nop ; nop  // OK!

    // Test (ZP),y
	:Mov16 ($fb),y : $1000    ; nop ; nop ; nop  // OK!
	:Mov16 $1000 : ($fb),y    ; nop ; nop ; nop  // OK!
	:Mov16 ($02),y : ($fb),y  ; nop ; nop ; nop  // OK!

    // Test (ZP,x) & (ZP),y
	:Mov16 ($02),y : ($fb,x)  ; nop ; nop ; nop  // OK!
	:Mov16 ($02,x) : ($fb),y  ; nop ; nop ; nop  // OK!

    // Test Absolute & Zero Page
    :Mov16 $1234 : $abcd      ; nop ; nop ; nop  // OK!
    :Mov16 $12 : $abcd        ; nop ; nop ; nop  // OK!
    :Mov16 $1234 : $ab        ; nop ; nop ; nop  // OK!

    // Test Absolute,x & Absolute,y
    :Mov16 $1234,x : $abcd,y  ; nop ; nop ; nop  // OK!
    :Mov16 $abcd,y : $1234,x  ; nop ; nop ; nop  // OK!

    // Test ZP,x & ZP,y
	// ZP,y, does not exist, should produce ABS,Y instead!
    :Mov16 $12,x : $ab,y      ; nop ; nop ; nop  // OK!
    :Mov16 $12,y : $ab,x      ; nop ; nop ; nop  // OK!


/*
    .pseudocommand Mov16 source : target {

        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //~              First LDA / STA is always working good!              ~
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        lda source
        sta target

        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //~                        Handle Source = IMM                        ~
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //~ This will remove the 2nd LDA if the hi/lobyte of the immediate    ~
		//~ value is equal, i.e.:                                             ~
        //~     lda #$01                                                      ~
        //~     sta $d020                                                     ~
        //~     lda #$01                                                      ~
        //~     sta $d021                                                     ~
        //~ will become:                                                      ~
        //~     lda #$01                                                      ~
        //~     sta $d020                                                     ~
        //~     sta $d021                                                     ~
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        .if(source.getType() == AT_IMMEDIATE) {
            .var a = >source.getValue()
            .var b = <source.getValue()
    		.if(a != b) {
    		    lda _16BIT_ARGUMENT(source)
			}
		}

        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //~                      Handle Source = (ZP,X)                       ~
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //~ This will add a INX if (ZP,X) to increase the index value instead ~
		//~ of the ZP value.                                                  ~
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		.if (source.getType() == -5) {  // AT_IZEROPAGEX <--- This seems to be a bug. Reported on csdb KA thread.
            inx
            lda source
        }

        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //~                      Handle Source = (ZP),Y                       ~
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //~ This will add a INY if (ZP),Y to increase the index value instead ~
		//~ of the ZP value.                                                  ~
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

        .if (source.getType() == AT_IZEROPAGEY) {
    		iny
            lda source
	    }

        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //~         Handle Source = ABS, ABS,X, ABS,y, ZP, ZP,X, ZP,Y         ~
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        .if (source.getType() == AT_ABSOLUTE) {
   		    lda _16BIT_ARGUMENT(source)
		}
        .if (source.getType() == AT_ABSOLUTEX) {
   		    lda _16BIT_ARGUMENT(source)
        }
        .if (source.getType() == AT_ABSOLUTEY) {
   		    lda _16BIT_ARGUMENT(source)
        }

        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //~                      Handle Target = (ZP,X)                       ~
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //~ This will add a INX if (ZP,X) to increase the index value instead ~
		//~ of the ZP value IF Source isn't (ZP,X) as well.                   ~
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		.if (target.getType() == -5) {  // AT_IZEROPAGEX <--- This seems to be a bug. Reported on csdb KA thread.
            .if (source.getType() != -5) {  // AT_IZEROPAGEX <--- This seems to be a bug. Reported on csdb KA thread.
			    inx
			}
			sta target
        }

        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //~                      Handle Target = (ZP),Y                       ~
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //~ This will add a INX if (ZP),Y to increase the index value instead ~
		//~ of the ZP value IF Source isn't (ZP),Y as well.                   ~
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		.if (target.getType() == AT_IZEROPAGEY) {  // AT_IZEROPAGEX <--- This seems to be a bug. Reported on csdb KA thread.
            .if (source.getType() != AT_IZEROPAGEY) {  // AT_IZEROPAGEX <--- This seems to be a bug. Reported on csdb KA thread.
			    iny
			}
			sta target
        }

        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //~         Handle Target = ABS, ABS,X, ABS,y, ZP, ZP,X, ZP,Y         ~
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		.if (target.getType() == AT_ABSOLUTE) {
    		sta _16BIT_ARGUMENT(target)
		}
		.if (target.getType() == AT_ABSOLUTEX) {
    		sta _16BIT_ARGUMENT(target)
		}
		.if (target.getType() == AT_ABSOLUTEY) {
    		sta _16BIT_ARGUMENT(target)
		}

    }


    .function _16BIT_ARGUMENT (argument) {
        .if (argument.getType()==AT_IMMEDIATE)
            .return CmdArgument(argument.getType(),>argument.getValue())
            .return CmdArgument(argument.getType(),argument.getValue()+1)
    }

*/