
    .macro SetScreenColor(ScreenColor, BorderColor) {
	    .if(ScreenColor==BorderColor) {
		    lda #ScreenColor
			sta $d020
			sta $d021
		} else {
		    lda #BorderColor
			sta $d020
		    lda #ScreenColor
			sta $d021
		}
	}