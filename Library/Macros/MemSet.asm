

    #if Include_MemSet
        .macro MemSet(Address, Quantity, FillByte) {
            jsr MemSet
            .word Address, Quantity ; .byte FillByte
        }
    #endif