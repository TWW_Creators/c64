

    #if Include_MemCpy
        .macro MemCpy(AddressFrom, AddressTo, Quantity) {
            jsr MemCpy
            .word AddressFrom, AddressTo, Quantity
        }
    #endif