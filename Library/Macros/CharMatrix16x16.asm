    #if Include_CharMatrix16x16
        .macro CharMatrix16x16(Address, Color) {
            lda #>Address
			ldx #<Address
			ldy #Color
            jsr CharMatrix16x16
        }
    #endif

