    //-------------------------------------------------------------------------
    // MemCpy - Subroutine to Copy Memory
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    //
    // This routine copies foreward and takes no consideration for
    // memory wraparound, overwriting itself or memory to be copied.
    //
	// Requirements:
    //     #define Include_MemCpy <- To be included in main code segment
    //     .import source "../Library/Library.asm" <- To import subroutines
	//
	// Uses:
    //     STACK-Frame for passing data:
	//         .word Address_From
	//         .word Address_To
	//         .word Quantity
    //     STACK-Frame Call Example:
	//         jsr MemCpy ; .word $2000, $0400, 1000
    //
	// Macro (to simplify):
	//     MemCpy(from,to,quantity)
	//         From     (U16)   - 16 bit address
	//         To       (U16)   - 16 bit address
	//         Quantity (U16)   - 16 bit dataamount (detects if HiByte = 0!)
	//     Macro Example:
	//         :MemCpy($2000,$0400,1000)
    //
    // Destroys:
	//     Accumulator
	//     Y-Register
    //     ZP_Register 1 (2 bytes) - Vector to Parameters
    //     ZP_Register 2 (2 bytes) - Vector to Fetch Address
	//     ZP_Register 3 (2 bytes) - Vector to Store Address
	//     ZP_Temp       (1 byte)  - Temporary storage for Quantity Lobyte
	//     2 STACK bytes for JSR/RTS from caller
	//     6 STACK bytes for passing parameters
	//     2 STACK bytes for STACK Frame
	//
	// Footprint:
	//     62 bytes excluding the STACK-Frame routine
	//
	//
    // Improvements:
	//     Make versions for copying to/from ZP or from ZP to ZP
	//     Add assertions for memory wrap/selt overwriting
	//     backwards copying (auto?)
	//     Using ZP Regs and indirect copying is slower than SMC.
	//         Add a switch to da a "faster" version? <- TBD!
    //-------------------------------------------------------------------------

        #importonce

MemCpy:
    jsr StackEntry
    ldy #$01
!:  lda (ZP_Reg1),y            // Address From Lo
    sta ZP_Reg2Lo-1,y
    iny
    cpy #$05                   // The consessive parameters will automatically be put into ZP_Reg2Hi, ZP_Reg3Lo & ZP_RegHi
    bne !-
    lda (ZP_Reg1),y            // Size Lo
    sta ZP_Temp                // Hold # of bytes less than 256 to copy
    iny  // Could this be moved into the STACK Frame routine instead as it will always be required?
    jsr StackFix               // Add Y to ZP_Reg1
    ldy #$00
    lda (ZP_Reg1),y            // Size Hi
    beq !LessThan256+
    tax
!:  lda (ZP_Reg2),y
    sta (ZP_Reg3),y
    iny
    bne !-
    inc ZP_Reg2Hi
    inc ZP_Reg3Hi
    dex
    bne !-
!LessThan256:
    lda ZP_Temp
    cmp #$00
    beq !End+
!:  lda (ZP_Reg2),y
    sta (ZP_Reg3),y
    iny
    cpy ZP_Temp
    bne !-
!End:
    jsr StackExit
    rts

    .import source "StackCall.asm"



