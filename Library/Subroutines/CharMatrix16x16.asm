    /*--------------------------------------------------------------------------
      16x16 Char Matrix
      ~~~~~~~~~~~~~~~~~

      Creates a 16 x 16 char matrix at the parameter specified.
                        @p 0...
                        aq!1.
                        be".
                        cs.
                        d.
                        .
                        .

      X:A = Screen Memory Location for the top left '@' in the char matrix.
    --------------------------------------------------------------------------*/

        #importonce

CharMatrix16x16:
    sty SMC_Color
    stx ZP_Reg1Lo
    stx ZP_Reg2Lo
    sta ZP_Reg1Hi
	and #%00000011
	clc
	adc #$d8
	sta ZP_Reg2Hi

    ldx #15                    // Lines
!NextLine:
    ldy #$00                   // Columns
!Loop:
    lda SMC_Color: #$00
	sta (ZP_Reg2),y
    lda SMC_BaseValue: #00
    sta (ZP_Reg1),y
    iny
    clc
    adc #16
	sta SMC_BaseValue
    bcc !Loop-

    lda ZP_Reg1Lo
    adc #39                    // CARRY is set
    sta ZP_Reg1Lo
    sta ZP_Reg2Lo
    bcc !+
        inc ZP_Reg1Hi
        inc ZP_Reg2Hi          // move to next Line

!:  inc SMC_BaseValue          // Increase base value
    dex
    bpl !NextLine-
    rts

