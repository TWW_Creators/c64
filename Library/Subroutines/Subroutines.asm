    /*-------------------------------------------------------------------------
      Creators Library Subroutines
	  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	  In order to use the subroutines, they have to be defined and the library
	  imported. Usually invoked with macros, see testsuite for examples.
    -------------------------------------------------------------------------*/

    #importif Include_CharMatrix16x16 "Subroutines/CharMatrix16x16.asm"
    #importif Include_MemCpy "Subroutines/MemCpy.asm"
    #importif Include_MemSet "Subroutines/MemSet.asm"
    #importif Include_StackCall "Subroutines/StackCall.asm"



