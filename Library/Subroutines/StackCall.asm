    //-------------------------------------------------------------------------
    // StackEntry & StackExit
    // ==========   =========
    //
    // Entry fetches .PC to byte after last jsr call and places it into ZP_Reg1
    // Parameters are then passed to the routine via indirect fetches (ZP_Reg1)
    // StackFix is called to push exit address after the parameters (by Y-Reg)
    // Exit adjust the ZP_Reg1 vector to return the .PC to last parameter
    //
    // The idea is to not spend a lot of memory passing parameters to subr.
    // but instead by using some overhead code to save bytes by calling
    // Subroutines throughout a code project.
    //
    // By using registers or ZP the preparation code for each call would grow
    // in each call. A MemSet routine needs two 16 bit values and a fillbyte
    // set up for each call to the Subroutine wich will quickly add up.
    //
    //
    // Example:
    //
    //     jsr Subroutine
    //         .byte Parameter1, Parameter2
    //     rts
    //
    // Subroutine:
    //     jsr StackEntry    // Arrange ZP_Reg1 to point to parameters
    //     // Fetch parameters
    //     ldy #$01
    //     lda (ZP_Reg1),y
    //     sta Parameter1
    //     iny
    //     lda (ZP_Reg1),y
    //     sta Parameter2
    //     jsr StackFix      // Adds Y-Reg to ZP_Reg1 to set return address
    //
    //     // Subroutine Code
    //     lda Parameter1
    //     clc
    //     adc Parameter2
    //     sta result
    //
    //     // Handle Subroutine Exit
    //     jsr Stackexit     // Push ZP_Reg1 back to the Stack
    //     rts
    //
    //  40 bytes added by the stack routines and 9 bytes in sub.
    //  Overhead in sub is situation dependent. Few bytes at worst.
    //  These routines are universal and can be used by all subroutines.
    //  The routine also handles variable parameters (like textstrings).
    //-------------------------------------------------------------------------

    #importonce


StackFix:
    tya
    clc
    adc ZP_Reg1Lo
    bcc !+
        inc ZP_Reg1Hi
!:  sta ZP_Reg1Lo
    rts

StackExit:
    clc
    .byte $24      // BIT OP Code (skips the sec after StackEntry)
StackEntry:
    sec

    // Store Return Address
    pla
    tax
    pla
    tay

    pla
    bcc !+

        // Entry Code
        sta ZP_Reg1Lo
        pla
        sta ZP_Reg1Hi        // Write pointer to first parameter to ZP_Reg1
        bcs !Done+

    // Exit Code
!:  pla                      // ignore old return address caller (before parameters)
    lda ZP_Reg1Hi
!Done:
    pha
    lda ZP_Reg1Lo
    pha                      // Restore return address of caller (last parameter byte)

    // Restore return address
    tya
    pha
    txa
    pha
    rts



