    //-------------------------------------------------------------------------
    // MemSet - Subroutine to Fill Memory with a byte
    // ======
    //
    //-------------------------------------------------------------------------

    #importonce

MemSet:
    jsr StackEntry
    ldy #$01
    lda (ZP_Reg1),y            // Address Lo
    sta ZP_Reg2Lo
    iny
    lda (ZP_Reg1),y            // Address Hi
    sta ZP_Reg2Hi
    iny
    lda (ZP_Reg1),y            // Quantity Lo
    sta ZP_Temp
    iny
    lda (ZP_Reg1),y            // Quantity Hi
    tax
    iny
    jsr StackFix               // Add Y to ZP_Reg1
    ldy #$00
    lda (ZP_Reg1),y            // Fillbyte


    cpx #$00
    beq !LessThan256+
    ldy #$00
!:  sta (ZP_Reg2),y
    iny
    bne !-
    inc ZP_Reg2Hi
    dex
    bne !-
!LessThan256:
    ldx ZP_Temp
    beq !End+
!:  sta (ZP_Reg2),y
    iny
    cpy ZP_Temp
    bne !-
!End:
    jsr StackExit
    rts


    .import source "StackCall.asm"
