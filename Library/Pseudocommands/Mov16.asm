    /*-------------------------------------------------------------------------
      Mov16                                           From KickAssembler manual
	  ~~~~~           Ported and expanded to handle all addressing modes by TWW


	  Uses A to copy 2 bytes from Source to Target.

      The Mov16 pseudocommand uses the Function "_16BIT_ARGUMENT" to fetch high
	  order bytes. <--- REMOVED FOR NOW for testingpurposes.


	  Example #1_1 (Immediate):
	      :Mov16 #$1234 : $fffe
	  Produces:
		  lda #$34
		  sta $fffe
		  lda #$12
		  sta $ffff

	  Example #1_2 (Immediate (Vector to label)):
	      :Mov16 #IRQ : $fffe
	  Produces:
		  lda #<IRQ
		  sta $fffe
		  lda #>IRQ
		  sta $ffff

	  Example #1_3 (Immediate (Same Hi/Lobyte in IMM Val.)):
	      :Mov16 #$0101 : $d020
	  Produces:
		  lda #$01
		  sta $d020
		  sta $d021

      Example #2 (Zero Page):
          :Mov16 ZP_Reg1 : ZP_Reg2
      Produces:
	      lda ZP_Reg1Lo  // $02
		  sta ZP_Reg2Lo  // $04
	      lda ZP_Reg1Hi  // $03
		  sta ZP_Reg2Hi  // $05

      Example #3 (Absolute):
          :Mov16 $1000 : $2000
      Produces:
	      lda $1000
		  sta $2000
		  lda $1001
		  sta $2001

      Example #4 (Absolute,x & Absolute,y):
          :Mov16 $1000,x : $2000,y
      Produces:
	      lda $1000,x
		  sta $2000,y
		  lda $1001,x
		  sta $2001,y

      Example #5 ( (Indirect ZP),y & (Indirect ZP,x) )
	      :Mov16 (ZP_Reg1,x) : (ZP_Reg2),y
      Produces:
	      lda (ZP_Reg1,x)  // $02
		  sta (ZP_Reg2),y  // $04
		  inx
	      lda (ZP_Reg1,x)  // $02
		  iny
		  sta (ZP_Reg2),y  // $04
		  This mode is special as we DO NOT want to increase the ZP Value,
		  but instead the index register value.


	  ToDo:
	    Assertions from CWA
		Figgure out how to avoid the fucking parsing issue with .IF statements.
      To Consider:
	    16 to 8 and 24/32 bits / use of other registers
    -------------------------------------------------------------------------*/


    .pseudocommand Mov16 source : target {

        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //~              First LDA / STA is always working good!              ~
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        lda source
        sta target

        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //~                        Handle Source = IMM                        ~
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //~ This will remove the 2nd LDA if the hi/lobyte of the immediate    ~
		//~ value is equal, i.e.:                                             ~
        //~     lda #$01                                                      ~
        //~     sta $d020                                                     ~
        //~     lda #$01                                                      ~
        //~     sta $d021                                                     ~
        //~ will become:                                                      ~
        //~     lda #$01                                                      ~
        //~     sta $d020                                                     ~
        //~     sta $d021                                                     ~
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        .if(source.getType() == AT_IMMEDIATE) {
//            .var a = >source.getValue()  // DISABLED FOR NOW AS IT FUCKS UP WHEN UNRESOLVED LABLES ARE PASSED!
//            .var b = <source.getValue()
//    		.if(a != b) {
    		    lda _16BIT_ARGUMENT(source)
//			}
		}

        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //~                      Handle Source = (ZP,X)                       ~
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //~ This will add a INX if (ZP,X) to increase the index value instead ~
		//~ of the ZP value.                                                  ~
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		.if (source.getType() == -5) {  // AT_IZEROPAGEX <--- This seems to be a bug. Reported on csdb KA thread.
            inx
            lda source
        }

        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //~                      Handle Source = (ZP),Y                       ~
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //~ This will add a INY if (ZP),Y to increase the index value instead ~
		//~ of the ZP value.                                                  ~
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

        .if (source.getType() == AT_IZEROPAGEY) {
    		iny
            lda source
	    }

        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //~         Handle Source = ABS, ABS,X, ABS,y, ZP, ZP,X, ZP,Y         ~
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        .if (source.getType() == AT_ABSOLUTE) {
   		    lda _16BIT_ARGUMENT(source)
		}
        .if (source.getType() == AT_ABSOLUTEX) {
   		    lda _16BIT_ARGUMENT(source)
        }
        .if (source.getType() == AT_ABSOLUTEY) {
   		    lda _16BIT_ARGUMENT(source)
        }

        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //~                      Handle Target = (ZP,X)                       ~
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //~ This will add a INX if (ZP,X) to increase the index value instead ~
		//~ of the ZP value IF Source isn't (ZP,X) as well.                   ~
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		.if (target.getType() == -5) {  // AT_IZEROPAGEX <--- This seems to be a bug. Reported on csdb KA thread.
            .if (source.getType() != -5) {  // AT_IZEROPAGEX <--- This seems to be a bug. Reported on csdb KA thread.
			    inx
			}
			sta target
        }

        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //~                      Handle Target = (ZP),Y                       ~
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //~ This will add a INX if (ZP),Y to increase the index value instead ~
		//~ of the ZP value IF Source isn't (ZP),Y as well.                   ~
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		.if (target.getType() == AT_IZEROPAGEY) {  // AT_IZEROPAGEX <--- This seems to be a bug. Reported on csdb KA thread.
            .if (source.getType() != AT_IZEROPAGEY) {  // AT_IZEROPAGEX <--- This seems to be a bug. Reported on csdb KA thread.
			    iny
			}
			sta target
        }

        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        //~         Handle Target = ABS, ABS,X, ABS,y, ZP, ZP,X, ZP,Y         ~
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		.if (target.getType() == AT_ABSOLUTE) {
    		sta _16BIT_ARGUMENT(target)
		}
		.if (target.getType() == AT_ABSOLUTEX) {
    		sta _16BIT_ARGUMENT(target)
		}
		.if (target.getType() == AT_ABSOLUTEY) {
    		sta _16BIT_ARGUMENT(target)
		}

    }


/*
    .pseudocommand Mov16 source : target {

        // First lda/sta is always good!
        lda source
        sta target

        // Check if source is IMM, (ZP,X) or (ZP),Y
		.if (source.getType() != AT_ABSOLUTE && source.getType() != AT_ABSOLUTEX && source.getType() != AT_ABSOLUTEY) {
            // Handles IMM, OK!
            .if (source.getType() == AT_IMMEDIATE) {
                // Check if Hi & Lobyte of Immediate value is equal, and if yes, skipp the LDA
                .var a = >source.getValue()
                .var b = <source.getValue()
				.if(a != b) {
    			    lda CmdArgument(source.getType(), >source.getValue())
                }
	        }
            // Handles (ZP,X), OK (with dubious workaround!!!)
            .if (source.getType() == -5) {  // AT_IZEROPAGEX <--- This seems to be a bug. Reported on csdb KA thread.
		        inx
                lda CmdArgument(source.getType(), source.getValue())
	        }
            // Handles (ZP),Y, OK!
            .if (source.getType() == AT_IZEROPAGEY) {
				iny
                lda CmdArgument(source.getType(), source.getValue())
	        }
        } else {
            // This handles ABS_ZP, ABS, ABS,X & ABS,Y
            lda CmdArgument(source.getType(), source.getValue()+1)
		}
        // We need to handle the 2nd STA if it's indirect indexed addressing AND we need to ensure we don't
		// increase indexes more than once if source also is the same indirect indexed type.
        .if (target.getType() == AT_IZEROPAGEY || target.getType() == -5) {  // AT_IZEROPAGEX <--- This seems to be a bug. Reported on csdb KA thread.
		    .if (target.getType() == AT_IZEROPAGEY) {
                // verify that source isn't (ZP),Y as well:
				.if (source.getType() != AT_IZEROPAGEY) {
                    iny
				}
			}
		    .if (target.getType() == -5) {  // AT_IZEROPAGEX <--- This seems to be a bug. Reported on csdb KA thread.
			    // Verify that source isn't (ZP,X) as well:
				.if (source.getType() != -5) {  // AT_IZEROPAGEX <--- This seems to be a bug. Reported on csdb KA thread.
                    inx
				}
			}
            // This handles the ID modes.
            sta CmdArgument(target.getType(), target.getValue())
        } else {
            // This handles all remaining addressing modes
            sta CmdArgument(target.getType(), target.getValue()+1)
		}
    }
*/