

		.pseudocommand SetGfxBank BitmapMemory : ScreenMemory {
			lda #[[[>ScreenMemory.getValue() & $3c00] << 2] | [[>BitmapMemory.getValue() & $3c00] >> 2]]
			sta $d018
			lda $dd00
			and #%11111100
			ora #[3 - [>ScreenMemory.getValue() >> 6]]
			sta $dd00
		}


