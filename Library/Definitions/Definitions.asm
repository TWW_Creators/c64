

    .const KOALA_TEMPLATE = "C64FILE, Bitmap=$0000, ScreenRam=$1f40, ColorRam=$2328, BackgroundColor = $2710"

    // Constants for setting the R6510 MPU IO Register
    .const RAM        = $34
    .const IO         = $35
    .const KERNAL     = $36  // Always has IO mapped in too
    .const BASIC      = $37  // Always has KERNAL and IO mapped in too


    .import source "Zeropage.asm"