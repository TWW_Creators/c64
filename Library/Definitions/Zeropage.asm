
    .const D6510 = $00  // MOS 6510 On-Chip Data Direction Register
                        // Default: #%XX101111 (1 = Output, 0 = Input)
    .const R6510 = $01  // MOS 6510 On-Chip 8-Bit I/O Register/Port
                        // Bit 7-6: Undefined
                        // Bit 5: Cassette Motor Control (0 = Off, 1 = On)
                        // Bit 4: Cassette Switch Sense (1 = Switch Closed)
                        // Bit 3: Cassette Data Output Line
                        // Bit 2: /CHAREN Signal (0 = Switch Char. ROM In)
                        // Bit 1: /HIRAM Signal (0 = Switch KERNAL ROM Out)
                        // Bit 0: /LORAM Signal (0 = Switch BASIC ROM Out)



    .var ZP = $02

    .const ZP_Reg1          = ZP ; .eval ZP+=2
        .const ZP_Reg1Lo    = ZP_Reg1
        .const ZP_Reg1Hi    = ZP_Reg1+1
    .const ZP_Reg2          = ZP ; .eval ZP+=2
        .const ZP_Reg2Lo    = ZP_Reg2
        .const ZP_Reg2Hi    = ZP_Reg2+1
    .const ZP_Reg3          = ZP ; .eval ZP+=2
        .const ZP_Reg3Lo    = ZP_Reg3
        .const ZP_Reg3Hi    = ZP_Reg3+1
    .const ZP_Temp          = ZP ; .eval ZP++




